const request = require('request-promise');

exports.searchUrl = async (url) => {
    return await request.get(url);
};
