module.exports = class TransformUrlResearchService {
    constructor(urlResearch) {
        this._urlResearch = urlResearch;
    }

    returnChildsRelative() {
        return this._urlResearch.getChilds()
            .map(child => `${child.pathname}${child.search}`);
    }

    retrieveFromOthersUrl() {
        return this._urlResearch.getOuterUrls().map(url => url.href);
    }

    retrieveUrls() {
        return {
            base: this._urlResearch.getUrl().href,
            relative: [
                ...this.returnChildsRelative(),
                ...this._urlResearch.getMayBeRelative()
            ],
            otherOrigin: this.retrieveFromOthersUrl()
        };
    }
};
