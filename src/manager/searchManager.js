const UrlResearch = require('../entity/urlResearch');
const { searchUrl } = require('../service/serviceSearch');
const utils = require('../utils/util');
const transformUrlResearchService = require('../service/transformUrlReseachService');
class SearchManager {
    async search(checkUrl) {
        const urlResearch = new UrlResearch(new URL(checkUrl));
        const result = await searchUrl(urlResearch.getHref());
        const urls = utils.findUrlsOnPath(result);
        for (const href of urls) {
            try {
                const url = new URL(href);
                urlResearch.addUrl(url);
            } catch (err) {
                urlResearch.addMaybeRelative(href);
            }
        }
        const _transformUrlResearchService = new transformUrlResearchService(urlResearch);
        return _transformUrlResearchService.retrieveUrls();
    }
}

module.exports = SearchManager;
