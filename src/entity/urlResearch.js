class UrlResearch {
    
    constructor(url) {
        this._url = url;
        this._childs = [];
        this._outerUrls = [];
        this._maybeRelative = [];
    }

    getUrl() {
        return this._url;
    }

    getHref() {
        return this._url.href;
    }

    getChilds() {
        return this._childs;
    }

    addChild(url) {
        this._childs.push(url);
        return this;
    }

    addUrl(url) {
        if (url.origin === this._url.origin) {
            this.addChild(url);
        } else {
            this.addOuterUrl(url);
        }

        return this;
    }

    getOuterUrls() {
        return this._outerUrls;
    }

    addOuterUrl(url) {
        this._outerUrls.push(url);
        return this;
    }

    getMayBeRelative() {
        return this._maybeRelative;
    }

    addMaybeRelative(url) {
        this._maybeRelative.push(url);
        return this;
    }
}

module.exports = UrlResearch;