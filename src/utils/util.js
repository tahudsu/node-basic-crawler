const cheerio = require('cheerio');

module.exports = utilsUrl = {
    urlChecker(checkUrl) {
        try {
            new URL(checkUrl);
        } catch (error) {
            throw new Error(error);
        }

        return true;
    },
    findUrlsOnPath(body) {
        const $ = cheerio.load(body);
        const href = [];
        $('body').find('a').map((i, el) => {
            if (el.type === 'tag' && el.name === 'a' && el.attribs.href) {
                href.push($(el).attr('href'));
            }
        });
        
        return href;
    }
};
