const args = require('yargs');
const urlSearch = require('./controller/urlSearch');
const utils = require('./utils/util');

const argv = args
    .usage('Usage: $0 <command> [url]')
    .command('search', 'search in url')
    .alias('u', 'url')
    .describe('u', 'must be a url')
    .argv;

(async () => {
    try {
        if (argv.url || argv.u) {
            const argUrl = argv.url || argv.u;
            if (utils.urlChecker(argUrl)) {
                exit = await urlSearch.searchByUrl(argUrl);
                console.log(JSON.stringify(exit));
                process.exit();
            }
        } else {
            throw new Error('please insert a param url');
        }
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
})();
