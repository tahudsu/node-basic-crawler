const SearchManager = require('../manager/searchManager');
exports.searchByUrl = async (checkUrl) => {
    return await new SearchManager().search(checkUrl);
};
